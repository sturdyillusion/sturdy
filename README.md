A public DNS resolver that focuses on privacy and speed. Built on the open-source foundations of Unbound and AdGuard Home.

---

### Features

- **Privacy-focused**: Zero logging, even statistics are disabled.
  
- **Fast and Reliable**: Server located in Singapore, ideal for users in India and Southeast Asia.
  
- **Secure**: DNSSEC enabled, no EDNS Client Subnet.
  
- **IPv6 Support**: Fully compatible with IPv6 addresses.
  
- **Multiple Protocols**: Supports DoT, DoH, and DoQ. DNSCrypt coming soon.